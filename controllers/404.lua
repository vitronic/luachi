--[[
          _                     _     _
         | |   _   _  __ _  ___| |__ (_)
         | |  | | | |/ _` |/ __| '_ \| |
         | |__| |_| | (_| | (__| | | | |
         |_____\__,_|\__,_|\___|_| |_|_|
Copyright (c) 2020  Díaz  Víctor  aka  (Máster Vitronic)
<vitronic2@gmail.com>   <mastervitronic@vitronic.com.ve>
]]--

local main	=	class('index');

function main:show()
	view:add_content('title',"Page not found")
	view:add_contents({
		css = {
			("/css/themes/%s/common/mustard-ui.min.css"):format(conf.theme.theme),
			("/css/themes/%s/public/404/404.css"):format(conf.theme.theme)
		}
	})
	local page = template.new(
		"/public/common/404.html"
	)
	view:generate(page)
end

function main:execute()
	http:header("Status: 404")
	main:show()
end

return main
